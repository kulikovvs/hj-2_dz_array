var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

console.info(' ');
console.info('Список студентов:');
for (var i = 0, imax = studentsAndPoints.length; i < imax; ++i) {
	console.log('Студент', studentsAndPoints[i], 'набрал', studentsAndPoints[++i], 'баллов');
}

console.info(' ');
console.info('Студент набравший максимальный балл:');

var maxIndex = 1, maxPts = studentsAndPoints[maxIndex];
for (var i = 1, imax = studentsAndPoints.length; i < imax; i = i + 2) {
	maxPts = Math.max(maxPts, studentsAndPoints[i]);
  if (maxPts === studentsAndPoints[i]) {
  	maxIndex = i;
  }
}	  
console.log('Студент', studentsAndPoints[maxIndex - 1], 'имеет максимальный бал', maxPts);

console.info(' ');
console.info('Новые студенты:');

studentsAndPoints.push('Николай Фролов', 0);
studentsAndPoints.push('Олег Боровой', 0);
for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	if (studentsAndPoints[i] === 'Николай Фролов') {
  	console.log('Студент', studentsAndPoints[i], 'набрал', studentsAndPoints[i + 1], 'баллов');
  } else if (studentsAndPoints[i] === 'Олег Боровой') {
  	console.log('Студент', studentsAndPoints[i], 'набрал', studentsAndPoints[i + 1], 'баллов');
  }
}

console.info(' ');
console.info('Увеличились баллы');

for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	if (studentsAndPoints[i] === 'Антон Павлович') {
  	studentsAndPoints[i + 1] += 10;
  	console.log('Студент', studentsAndPoints[i], studentsAndPoints[i + 1], 'баллов');
  } else if (studentsAndPoints[i] === 'Николай Фролов') {
    studentsAndPoints[i + 1] += 10;
  	console.log('Студент', studentsAndPoints[i], studentsAndPoints[i + 1], 'баллов');
  }
}

console.info(' ');
console.info('Студенты не набравшие баллов:');

for (var i = 1, imax = studentsAndPoints.length; i < imax; i = i + 2) {
	if (studentsAndPoints[i] === 0) {
  	console.log(studentsAndPoints[i - 1]);
  }
}

console.info(' ');
console.info('Список студентов набравших баллы:');

var deletedStudents;
for (var i = 1, imax = studentsAndPoints.length; i < imax; ++i) {
	if (studentsAndPoints[i] === 0) {
  	deletedStudents = studentsAndPoints.splice(--i, 2);
  } else {++i;}
}
for (var i = 0, imax = studentsAndPoints.length; i < imax; ++i) {
	console.log('Студент', studentsAndPoints[i], 'набрал', studentsAndPoints[++i], 'баллов');
}